from churchrepl.repl.parser import LambdaSemantics
from churchrepl.repl.lexer import Lambda, Var


class TestParserSemantics:
    def __init__(self):
        self.is_setup = False
        self.test_lines = []

    def setUp(self):
        assert not self.is_setup
        self.is_setup = True

    def tearDown(self):
        assert self.is_setup
        self.is_setup = False

    def test_semantics_line(self):
        assert LambdaSemantics().line('Test') == 'Test'

    def test_semantics_define(self):
        ls = LambdaSemantics()
        assert ls.define(["@", Var('ID'), ":",
                         Lambda(Var('x'), Var('x'))]) is None

    def test_semantics_function(self):
        assert LambdaSemantics().function('Test') == 'Test'

    def test_semantics_lambda_(self):
        assert LambdaSemantics()\
                   .lambda_(['\\', Var('x'), '.', Var('x')]).__str__() == \
                   'lambda x: x'

    def test_semantics_application(self):
        assert LambdaSemantics()\
                .application(['(', Lambda(Var('x'), Var('x')),
                             Lambda(Var('x'), Var('x')), ')']).__str__() == \
                '(lambda x: x)(lambda x: x)'

    def test_semantics_expr(self):
        assert LambdaSemantics().expr('Test') == 'Test'

    def test_semantics_variable(self):
        assert LambdaSemantics().variable('x').name == 'x'

    def test_semantics_alias(self):
        assert LambdaSemantics().alias('x').name == 'x'
