from churchrepl.repl import lex
from churchrepl.repl.lexer import Lambda, Var, Apply, Alias


class TestChurchRepl:
    def __init__(self):
        self.is_setup = False
        self.test_lines = []

    def setUp(self):
        assert not self.is_setup
        self.is_setup = True

    def tearDown(self):
        assert self.is_setup
        self.is_setup = False

    def test_lex_single(self):
        defs = [
            ('ZERO', Lambda(Var('f'), Lambda(Var('x'), Var('x')))),
            ('SUCC', Lambda(Var('n'),
                            Lambda(Var('f'),
                                   Lambda(Var('x'),
                                          Apply(Var('f'),
                                                Apply(Apply(Var('n'),
                                                            Var('f')),
                                                      Var('x'))))))),
            ('ONE', Apply(Alias('SUCC'), Alias('ZERO'))),
        ]
        lambda_list = [Apply(Alias('SUCC'), Alias('ONE')),
                       Apply(Alias('SUCC'), Apply(Alias('SUCC'),
                                                  Alias('ONE')))]
        ret = lex(lambda_list, defs)
        assert str(ret[0]) == \
            '(lambda n: lambda f: lambda x: f(n(f)(x)))'  \
            '((lambda n: lambda f: lambda x: f(n(f)(x)))' \
            '(lambda f: lambda x: x))'

        assert str(ret[1]) == \
            '(lambda n: lambda f: lambda x: f(n(f)(x)))'  \
            '((lambda n: lambda f: lambda x: f(n(f)(x)))' \
            '((lambda n: lambda f: lambda x: f(n(f)(x)))' \
            '(lambda f: lambda x: x)))'
