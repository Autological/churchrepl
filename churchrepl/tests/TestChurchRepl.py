import mock
import sys
import churchrepl
from os import path
from io import BytesIO


class TestChurchRepl:
    def __init__(self):
        self.is_setup = False
        self.test_lines = []

    def setUp(self):
        assert not self.is_setup
        here = path.abspath(path.dirname(__file__))
        filename = path.join(here, 'TestChurchRepl.l')
        self.test_lines = '\n'.join(open(filename).readlines())
        self.is_setup = True

    def tearDown(self):
        assert self.is_setup
        self.is_setup = False

    def test_set_args_v(self):
        args = churchrepl.set_args(["-v"])
        assert args.verbose
        assert args.file is None

    def test_set_args_f_single(self):
        args = churchrepl.set_args(["-f", "test.l"])
        assert args.file[0] == "test.l"
        assert not args.verbose

    def test_set_args_f_multiple(self):
        args = churchrepl.set_args(["-f", "test.l", "program.l"])
        assert args.file[0] == "test.l"
        assert args.file[1] == "program.l"
        assert not args.verbose

    def test_set_args_v_f_multiple(self):
        args = churchrepl.set_args(["-v", "-f", "test.l", "program.l"])
        assert args.file[0] == "test.l"
        assert args.file[1] == "program.l"
        assert args.verbose

    def test_repl_mock(self):
        args = churchrepl.set_args([])
        out = BytesIO()
        sys.stdout = out
        with mock.patch('churchrepl.input', side_effect=self.test_lines):
            try:
                churchrepl.repl_eval("", args)
                output = out.getvalue().strip()
                assert "!" not in output
            except StopIteration:
                pass
