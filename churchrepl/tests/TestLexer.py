from collections import Counter
from churchrepl.repl.lexer import (
    Lambda, Var, Apply, Alias,
    recursive_none_filter, replace_alias,
)


class TestLexicalEvaluation:
    def __init__(self):
        self.is_setup = False

    def setUp(self):
        assert not self.is_setup
        self.identityLambda = Lambda(Var(u'x'), Var(u'x'))
        self.zeroLambda = Lambda(Var(u'f'), Lambda(Var(u'x'), Var(u'x')))
        self.selfLambda = Lambda(Var(u's'), Apply(Var(u's'), Var(u's')))
        self.complexLambda = Lambda(Var(u'n'),
                                    Lambda(Var(u'f'),
                                           Lambda(Var(u'x'),
                                                  Apply(Var(u'f'),
                                                        Apply(Apply(Var(u'n'),
                                                                    Var(u'f')),
                                                              Var(u'x'))))))
        self.is_setup = True

    def toint(self, n):
        return n(lambda x: x + 1 if isinstance(x, int) else
                 (lambda x: lambda a: a(x) + 1 if x is not None else 0))(0)

    def test_lambda_init(self):
        assert Lambda(None, Var('y')).arg.name == 'x'
        assert Lambda(None, Var('y')).body.name == 'x'

        l = Lambda(Var('y'), None)
        assert l.arg.name == 'x'
        assert l.body.name == 'x'

        l = Lambda(Var('y'), Var('z'))
        assert l.arg.name == 'y'
        assert l.body.name == 'z'

    def test_lambda_evalstr(self):
        assert str(self.identityLambda) == 'lambda x: x'

    def test_var_evalstr(self):
        v = Var('x')
        assert str(v) == 'x'

    def test_apply_init(self):
        a = Apply(Var('f'), Var('x'))
        assert a.func.name == 'f'
        assert a.arg.name == 'x'

    def test_apply_evalstr(self):
        assert str(self.selfLambda) == 'lambda s: s(s)'

    def test_evalstr_zero(self):
        assert str(self.zeroLambda) == 'lambda f: lambda x: x'
        assert self.toint(eval(str(self.zeroLambda))) == 0

    def test_evalstr_identity(self):
        assert self.toint(eval(str(self.identityLambda))) == 1

    def test_evalstr_complex(self):
        assert str(self.complexLambda) == \
               'lambda n: lambda f: lambda x: f(n(f)(x))'
        assert self.toint((eval(str(self.complexLambda)))
                          (eval(str(self.zeroLambda)))) == 1

    def test_recursive_none_filter(self):
        assert Counter(recursive_none_filter(None)) == Counter(None)
        assert Counter(recursive_none_filter([None])) == Counter(None)
        assert Counter(recursive_none_filter([[None]])) == Counter(None)
        assert Counter(recursive_none_filter([None, 1])) == Counter([1])
        assert recursive_none_filter([None, [1]]) == [[1]]

    def test_replace_alias(self):
        defs = [
            ('ZERO', Lambda(Var('f'), Lambda(Var('x'), Var('x'))))
        ]
        al0 = Lambda(Var('f'), Lambda(Var('x'), Var('x')))
        al1 = Lambda(Var('n'), Alias('ZERO'))
        aa0 = Lambda(Var('s'), Apply(Var('s'), Var('s')))
        aa1 = Lambda(Var('s'), Apply(Apply(Alias('ZERO'), Var('s')),
                                     Apply(Alias('ZERO'), Var('s'))))

        rl0 = replace_alias(al0, defs)
        rl1 = replace_alias(al1, defs)
        ra0 = replace_alias(aa0, defs)
        ra1 = replace_alias(aa1, defs)

        assert str(rl0[0]) == "lambda f: lambda x: x"
        assert str(rl1[0]) == "lambda n: lambda f: lambda x: x"
        assert str(ra0[0]) == "lambda s: s(s)"
        assert str(ra1[0]) == \
            "lambda s: (lambda f: lambda x: x)(s)((lambda f: lambda x: x)(s))"

        assert rl0[1] is False
        assert rl1[1] is True
        assert ra0[1] is False
        assert ra1[1] is True

    def test_lex(self):
        pass
