from churchrepl.repl.lexer import Var, Apply, Lambda
from churchrepl.repl.redux import contains_lambda
from churchrepl.repl import redux_alpha


class TestRedux:
    def __init__(self):
        self.is_setup = False
        self.x = None
        self.y = None
        self.z = None

    def setUp(self):
        assert not self.is_setup
        self.x = Var('x')
        self.y = Var('y')
        self.z = Var('z')
        self.is_setup = True

    def tearDown(self):
        assert self.is_setup
        self.is_setup = False

    def test_contains_lambda(self):
        assert contains_lambda('x', Lambda(self.x, self.x))
        assert contains_lambda('x', Lambda(self.x, self.y))
        assert not contains_lambda('y', Lambda(self.x, self.x))
        assert not contains_lambda('y', Lambda(self.x, self.y))

        assert contains_lambda('x', Lambda(self.y, Lambda(self.x, self.x)))
        assert contains_lambda('x', Lambda(self.y, Lambda(self.x, self.x)))
        assert not contains_lambda('y', Lambda(self.z, Lambda(self.x, self.x)))
        assert not contains_lambda('y', Lambda(self.z, Lambda(self.x, self.y)))

        assert contains_lambda('x', Apply(Lambda(self.y, self.y),
                                          Lambda(self.x, self.x)))
        assert contains_lambda('y', Apply(Lambda(self.y, self.y),
                                          Lambda(self.x, self.x)))
        assert not contains_lambda('z', Apply(Lambda(self.y, self.y),
                                              Lambda(self.x, self.x)))

    def test_alpha(self):
        mapping = [
            (Var('x'), 'x'),
            (Apply(Var('x'), Var('y')), 'x(y)'),
            (Lambda(Var('x'), Var('x')), 'lambda x: x'),
            (Apply(Lambda(Var('x'), Var('x')), Lambda(Var('y'), Var('y'))),
             '(lambda x: x)(lambda y: y)'),
            (Lambda(Var('x'), Lambda(Var('x'), Var('x'))),
             'lambda x1: lambda x: x'),
            (Apply(Lambda(Var('x'),
                          Apply(Var('x'),
                                Lambda(Var('y'),
                                       Apply(Var('x'),
                                             Lambda(Var('x'),
                                                    Var('x')))))),
                   Lambda(Var('s'), Apply(Var('s'), Var('s')))),
             '(lambda x1: x1'
             '(lambda y: x1'
             '(lambda x: x)))'
             '(lambda s: s(s))')
        ]
        for map in mapping:
            assert str(redux_alpha([map[0]])[0]) == map[1]
