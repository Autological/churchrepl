@ID: λx.x
@SELF: λs.(s s)
@TRUE: λf.λx.f
@FALSE: λf.λx.x
@COND: λc.λp.λq.((c p) q)
@SUCC: λn.λf.λx.(f ((n f) x))
@ADD: λm.λn.λf.λx.((m f) ((n f) x))
@MUL: λm.λn.λx.(m (n x))
@POW: λm.λn.(n m)

@ISZ: λn.((n λk.FALSE) TRUE)
@PRED: λn.λf.λx.(((n λg.λh.(h (g f))) λu.x) λu.u)
@SUB: λm.λn.((n PRED) m)
@N_0: λf.λx.x
@N_1: (SUCC N_0)
@N_2: (SUCC N_1)
@N_3: (SUCC N_2)
@N_4: (SUCC N_3)
@N_5: (SUCC N_4)
@N_6: (SUCC N_5)
@N_7: (SUCC N_6)
@N_8: (SUCC N_7)
@N_9: (SUCC N_8)

@Y: λs.(λb.(s (b b)) λb.(s (b b)))
@_FAC: λf.λx.(((ISZ x) N_1) ((MUL x) (f (PRED x))))
@Y_FAC: (Y _FAC)
@FAC: λn.(((n λc.λq.((q (SUCC (c TRUE))) ((MUL (c TRUE)) (c FALSE)))) λq.(SUCC N_0)) FALSE)