(* meta *)
program     = {line};
line        = (define | function);
define      = "@" alias ":" function;

(* lambdas *)
function    = lambda | application;
lambda      = ("λ" | "\") variable "." expr;
application = '(' expr  expr ')';
expr        = (lambda | application | variable | alias ) ;
variable    = /[a-z]/;
alias       = /[_A-Z][_A-Z0-9]*/;
